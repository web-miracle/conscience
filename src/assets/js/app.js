import './bootstrap';

// import Swiper from 'swiper';
import $ from 'jquery';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';

window.addEventListener( 'load', (event) => {
	$.get('https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.2/css/swiper.min.css',
		function(data) {
			var style = document.createElement('style');
			style.innerText = data;
			document.body.appendChild(style);
			
			const swiper = new Swiper('.specialists .swiper-container', {
				slidesPerView: 3,
				spaceBetween: 20,
				loop: true,
				grabCursor: true,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					850: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				}
			});
			
			const swiper2 = new Swiper('.orders .swiper-container', {
				slidesPerView: 3,
				loop: true,
				direction: 'vertical',
				allowTouchMove: false,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
			
			const swiper3 = new Swiper('.question .swiper-container', {
				slidesPerView: 2,
				loop: true,
				direction: 'vertical',
				allowTouchMove: false,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					850: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				}
			});

			if (window.innerWidth <= 850) {
				const swiper4 = new Swiper('.whatwedoMobile .swiper-container', {
					slidesPerView: 2,
					spaceBetween: 40,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
				});

				const swiper5 = new Swiper('.feedback .swiper-container', {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoHeight: true
				});

				const swiper6 = new Swiper('.video-call .swiper-container', {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoHeight: true
				});

				const swiper7 = new Swiper('.firms .swiper-container', {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoHeight: true
				});

				const swiper8 = new Swiper('.repairing .swiper-container', {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoHeight: true
				});

				const swiper9 = new Swiper('.manufacturer .swiper-container', {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoHeight: true
				});
			}
		}
	);


	$('.accordion').on('show.bs.collapse', function (e) {
		$(e.target).parent().addClass('accordion-miracle-show');
	})

	$('.accordion').on('hide.bs.collapse', function (e) {
		$(e.target).parent().removeClass('accordion-miracle-show');
	})


	const lazy = new LazyLoad({
			elements_selector: ".lazyload"
		});

	var cityModal = document.getElementById('cityModal');
	var input = cityModal.querySelector('#exampleInputEmail1');

	input.addEventListener('input', function(event) {
		var cities = cityModal.querySelectorAll('.modal-body p');
		const val = event.target.value.toLowerCase();

		cities.forEach((city) => {
			const reg = new RegExp(val, "g");
			if (reg.exec(city.innerText.toLowerCase()) !== null) {
				city.style.display = '';
			} else {
				city.style.display = 'none';
			}
		});
	});

	var cities = cityModal.querySelectorAll('.modal-body a');

	cities.forEach((link) => {
		const city = link.querySelector('p');
		const val = city.innerText;

		link.addEventListener('click', (event) => {
			event.preventDefault();
			$('[data-target="#cityModal"]').find('u').text(val);
			$('#cityModal').modal('hide');
		});
	});
});